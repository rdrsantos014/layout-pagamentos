<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>Faturas</title>
</head>
<body>
  <header  class="header container">
    <div class="header-container">
      <div class="header-logo">
        <img src="samarel.png">
      </div>
      <div>
          <h3 style="color: #004163;">Serviços de Assistência Médica e Ambulatorial LTDA</h3>
      </div>
    </div>
  </header>
  
  <div class="cliente-info container">
    <h5>Rua: Machado de Assis, N° 403 - Office Center</h5>
    <h5>Centro, Mossoró / RN </h5>
    <h5>CNPJ: 14.775.280/0001-14
    </h5>
    <h5>
      <strong>Unidade :</strong> UNIDADE 1 <?php //echo $resultado2->nomeestabelecimento; ?>
      <strong>Setor:</strong> SETOR 1 <?php //echo $setorcabecalho; ?> </h5>
    <h5>
      <strong>NFSE:</strong> EXEMPLO <?php //echo $resultado2->nfse; ?>
      <strong>VALOR:</strong> 0,00 <?php //echo number_format($nfse_cabecalho, 2, ',', '.'); ?> 
      <strong>Competência :</strong>ABRIL/2021<?php //echo mb_strtoupper($resultado2->data_competencia);?> </h5>
  </div>

  <div class="titulo">
    <h2>Relatório de Pagamento</h2>
  </div>

  <div class="tabela-faturas container">
    <div class="fatura-item">
      <div class="fatura-header">
        <h2 class="fatura-cliente">NOME: FULANO PEREIRA COSTA</h2>
        <div class="dados-banco">
          <p><span>AG:</span> 0000-0</p>
          <p><span>CC:</span> 0000-0</p>
          <p><span>BANCO:</span> BANCO EXEMPLO</p>
          <p><span>CPF:</span> 0000000000</p>
        </div>
      </div>
      <table class="fatura-tabela">
        <thead>
          <th>TIPO</th>
          <th>QUANTIDADE</th>
          <th>VALOR R$</th>
        </thead>
        <tbody>
          <tr>
            <td>HORA</td>
            <td>00:00</td>
            <td>R$ 00,00</td>
          </tr>
          <tr>
        </tbody>
      </table>
      <div class="total-pagar">
        <h2>Total a pagar: R$ 00,00</h2>
      </div>
    </div>


  <div class="container total-geral">
    <h3 class="text-right"> 
      <bold style="font-weight:700">Total a pagar geral : R$ 0,00  <?php //echo ' R$:' . number_format($somatotal, 2, ',', '.'); ?> </bold>      
  </h3>
  </div>
</body>
</html>